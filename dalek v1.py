## -*_ coding: utf-8 -*-

""" 
Salut Benjamin!
voici ou je suis rendu....    j'ai reussi à faire bouger le docteur et les daleks en utilisant les objets
Aussi, c'est la "vue" qui prend les inputs (mais je ne vérifie pas pour l'instant si c'est un move légal).
Bref, il reste tout plein de choses mais ça commence à être un peu plus clair pour moi.   J'espère que tu réussiras
à comprendre mon code.  Oublie pas de faire un push sur ton code demain comme ça on ne dédoublera pas le travail.
Bonne chance!
"""


import random

DIMX=8
DIMY=8

dicodirection = { "1" : [-1,-1], 
                  "2" : [ 0,-1],
                  "3" : [ 1,-1],
                  "4" : [-1, 0],
                  "5" : [ 0, 0],
                  "6" : [ 1, 0],
                  "7" : [-1, 1],
                  "8" : [ 0, 1],
                  "9" : [ 1, 1],
                  "z" : [ 'z', 'z'],
                  "t" : [ 't', 't']}

class Vue():
    def __init__(self,parent):
        self.parent=parent
        
    def imprimeairedejeu(self, aire):

        for i in range(DIMX):
            for j in range(DIMY):
                print(aire[i][j], end='')
            print()          
            
    def menu_initial(self):
        #print("Pour débuter une nouvelle partie, choisir le mode: 1=facile, 2=normal, 3= difficile.")
        #choix_mode = input()
        score=0
        niveau=1
        nbDaleks=5
        nbFerraille=0
        
    def menu_partie(self):
        print("déplacement / zapper / téléporteur : ")
        touche_saisie=input()
        action_saisie = dicodirection[touche_saisie]

        Tour.prochaintour(action_saisie)
        
        
        
# vraiment pas sur qu'on a besoin d'une classe de la sorte....   ?????  
# mais c'est surement plus clair comme ça.      
class Tour():
    def __init__(self):
        pass
    
    def prochaintour(action_saisie):
    #1 action Docteur
#     self.modele.docteur.action(action_saisie)
        c.action_saisie(action_saisie)
    
    #2 deplacements des Daleks
        for i in c.modele.dalek:
            Dalek.deplacement(i)
        
    #3 verifier collisions Dalek - Dr (fin de partie)
    
    #4 verifier collision interDalek
    
    #5 verifier collision avec tas ferraille
    
    #6 verifier s'il reste des Daleks
       
    #7 mise à jour du jeu
        c.modele.miseajourjeu()  
        
            
class Docteur():
    def __init__(self,x,y):
        self.x=x
        self.y=y
                   
    def zapper(self):
        print("ZAPPER!")
        # à faire...
    
    def teleporter(self):
        print("Téléporteur!")
        # à faire...
                        
    def action(self, action_saisie):

        print(action_saisie)

        if action_saisie[0]=='z':
            Docteur.zapper(self)
        elif action_saisie[0]=='t':
            Docteur.teleporter(self)
            
        else:
            
            self.x+=action_saisie[0]
            if (self.x >= DIMX):
                self.x-=1
            if (self.x < 0):
                self.x+=1   
                
            self.y-=action_saisie[1]
            if (self.y >= DIMY):
                self.y-=1
            if (self.y < 0):
                self.y+=1 
            # bon ici je triche un peu, j'empeche de dépasse les limites au lieu de refuser un deplacment illegal...
            # comme le deplacement se fait dans la "VUE", je ne sais pas trop comment verifier si ce deplacement est 
            # legal ou pas.  Pour l'instant ça marche et ca ne plante pas au moins.        



class Dalek():
    def __init__(self,x,y):
        self.x=x
        self.y=y
        
    def deplacement(self):
        if self.x < c.modele.docteur.x:
            self.x += 1
        elif self.x > c.modele.docteur.x:
            self.x -= 1
        if self.y < c.modele.docteur.y:
            self.y += 1
        elif self.y > c.modele.docteur.y:
            self.y -= 1            
            
                        
#class Ferraille():
#    def __init__(self,x,y):
#        self.x=x
#        self.y=y
    
class Jeu(): 
    def __init__(self):
#         self.dimx=dimx     # remplacer dimx et dimy par constantes!
#         self.dimy=dimy
        self.dalek=[]
        #self.ferraille=[]
        self.airedejeu=None
        self.preparejeu()
    

        
    def preparejeu(self):
        
        airedejeu = []    # doit être modifier en objet !?  voir énoncé à la fin...
        
        #Board
        for i in range(DIMY):
            ligne=[]
            for j in range(DIMX):
                ligne.append(".")
            airedejeu.append(ligne)
        self.airedejeu=airedejeu     
        
        #Dalek
        pos=[]
        nbDalek=5
        while nbDalek:
            x=random.randrange(DIMX)
            y=random.randrange(DIMY)
            if [x,y] not in pos:
                pos.append([x,y])
                nbDalek-=1
                self.dalek.append(Dalek(x, y))  # on crée des objets Dalek !
        
#         for i in pos:
# #             self.dalek.append(Dalek(i[0],i[1])) 
#             iD = 0 # indice Dalek
#             self.dalek[iD] = Dalek(x, y)
#             iD +=1
        
        for i in self.dalek:
            self.airedejeu[i.y][i.x]="X"
        
        
        #Docteur
        jettonDocteur=1
        while jettonDocteur:
            x=random.randrange(DIMX)
            y=random.randrange(DIMY)    
            if [x,y] not in pos:
                pos.append([x,y])
                jettonDocteur-=1

        self.docteur=Docteur(x,y)   # on crée l'objet Docteur ici avec les bonnes coordonnées
                       
        self.airedejeu[self.docteur.y][self.docteur.x]="O"   # pas de sur de comprendre pourquoi les x et les y sont inversés...
                     
        return airedejeu
    
    def miseajourjeu(self):
    
        #Board
        for i in range(DIMY):
            for j in range(DIMX):
                self.airedejeu[j][i]= '.'
 
        self.airedejeu[self.docteur.y][self.docteur.x]="O"
        
        for i in self.dalek:
            self.airedejeu[i.y][i.x]="X"
    
    
class Controlleur():
    def __init__(self):
        self.modele=Jeu()
        self.vue=Vue(self)
        
    def action_saisie(self, action_saisie): # transmettre l'action recue de la vue au modele
        self.modele.docteur.action(action_saisie)
        

    
if __name__ == '__main__':
    
    gameon = True   # par opposition à gameover...
    c=Controlleur()
    
    c.vue.menu_initial()
    
    while (gameon):
        c.vue.imprimeairedejeu(c.modele.airedejeu)
        c.vue.menu_partie()
 

    
    
    
    
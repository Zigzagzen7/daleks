## -*_ coding: utf-8 -*-

# B41 TP1 : jeu de Daleks
# par Benjamin Barry et Alexandre Marcil
# 27 août 2018

import random
from random import randrange

DIMX=12
DIMY=12

dicodirection = { "1" : [-1,-1], 
                  "2" : [ 0,-1],
                  "3" : [ 1,-1],
                  "4" : [-1, 0],
                  "5" : [ 0, 0],
                  "6" : [ 1, 0],
                  "7" : [-1, 1],
                  "8" : [ 0, 1],
                  "9" : [ 1, 1],
                  "z" : [ 'z', 'z'],
                  "t" : [ 't', 't'],
                  "r" : [ 'r', 'r'],
                  "d" : [ 'd', 'd']}

class Vue():
    def __init__(self,parent):
        self.parent=parent
        
    def imprimeairedejeu(self, aire):

        print("Score: ", c.modele.score, "   Munitions: ", c.modele.munition)    
        for i in range(DIMX):
            for j in range(DIMY):
                print(aire[i][j], end='')
            print()          
            
    def menu_initial(self):
        print("Pour débuter une nouvelle partie, choisir le mode(1=facile, 2=normal, 3= difficile)(h pour HIGHSCORES): ")
        c.modele.difficulty = input()
        while c.modele.difficulty not in ["1","2","3","h","q"]:
            c.modele.difficulty = input("Choix invalide, réessayer: ")
        
    def menu_partie(self):
        print("déplacement / zapper / téléporteur : ")
        touche_valide = False
        
        while touche_valide == False:
            touche_saisie=input()
            
            if touche_saisie not in dicodirection:
                print("choix invalide...  reessayer: ")
            else:
                action_saisie = dicodirection[touche_saisie]
            
                if touche_saisie == "z" and c.modele.munition < 1:
                    print("Il ne vous reste plus de munition au zapper!")

                elif touche_saisie in ["z","t","d","r"]:
                    if touche_saisie != "r":
                        touche_valide = True
                    else:
                        for i in c.modele.dalek:
                            if c.modele.docteur.x - i.x in range(-1,2) and c.modele.docteur.y -i.y in range(-1,2):
                                print("Vous allez mourir!Bougez!")
                                touche_valide=False
                                break
                            else:
                                touche_valide=True
                elif int(touche_saisie) in range(1, 10):
            
                    docX= c.modele.docteur.x+action_saisie[0]  # variables temp pour vefifier si position du docteur valide
                    docY= c.modele.docteur.y-action_saisie[1]
                    touche_valide = True
                
                    if (docX < 0 or docX > DIMX-1): 
                        print("coup invalide")
                        touche_valide = False
                    if (docY < 0 or docY > DIMY-1):
                        print("coup invalide")
                        touche_valide = False
                  
                    # verif si le doc va sur ferraille   
                    for i in c.modele.ferraille:
                        if docX == i.x and docY == i.y:
                            print("coup invalide: vous ne pouvez pas aller sur la ferraille!")
                            touche_valide = False
                elif touche_saisie == "r":
                            pass
        return action_saisie
    
    def showscores(self):
        
        c.modele.hiscores.sort(key=None, reverse=True)      
        for i in c.modele.hiscores:
            if i != 0:
                print(i[1]," : ",i[0])
              
class Tour():
    def __init__(self):
        pass
    
    def prochaintour(action_saisie):
        #1 action Docteur
        c.action_saisie(action_saisie)
    
        #2 deplacements des Daleks
        for i in c.modele.dalek:
            Dalek.deplacement(i)
        
        #3 verifier collisions Dalek - Dr (fin de partie)   
        for i in c.modele.dalek:
            if c.modele.docteur.x == i.x and c.modele.docteur.y ==i.y:
                c.modele.gameon=False
                c.modele.miseajourjeu()
                c.modele.airedejeu[i.y][i.x]=" ☹ "
                c.vue.imprimeairedejeu(c.modele.airedejeu)
                print("GAME OVER")

                tempscore=[0," "]
                if len(c.modele.hiscores) < 3 and c.modele.score > 0:
                    tempscore[0]=c.modele.score
                    tempscore[1]=(input("Entrez votre nom: "))
                    c.modele.hiscores.append(tempscore)
                else:
                    for i in c.modele.hiscores:
                        if c.modele.score > i[0]:
                            tempscore[0]=c.modele.score
                            tempscore[1]=(input("Entrez votre nom: "))
                            c.modele.hiscores.pop()
                            c.modele.hiscores.append(tempscore)
                
                c.vue.showscores()
                break

        #4 verifier collision interDalek   
        collisionDalek=[]
        for i in c.modele.dalek:
            for j in c.modele.dalek:
                if (i.x == j.x)and(i.y == j.y):
                    if i != j and i not in collisionDalek:
                        collisionDalek.append(i)
                        c.modele.ferraille.append(i)
        for i in collisionDalek:
            c.modele.dalek.remove(i)
            c.modele.score += 5
        del collisionDalek[:]
        
        #5 verifier collision avec tas ferraille
        collisionFerraille=[]
        for m in c.modele.dalek:
            for j in c.modele.ferraille:
                if m.x==j.x and m.y == j.y:
                    if m !=j and m not in collisionFerraille:
                        collisionFerraille.append(m)                  
        for i in collisionFerraille:
            c.modele.dalek.remove(i)
            c.modele.score += 5
        del collisionFerraille[:]
    
        #6 verifier s'il reste des Daleks
        if not c.modele.dalek:
            c.modele.munition+=1
            c.modele.niveau += 1
            
            c.modele.preparejeu()
            
        #7 mise � jour du jeu
        c.modele.miseajourjeu()  
         
class Docteur():
    def __init__(self,x,y):
        self.x=x
        self.y=y
        self.runtoend=False
        self.runtodead=False
                   
    def zapper(self):
        print("ZAPPER!")
        vaporiser=[]
        if c.modele.munition > 0:
            for i in c.modele.dalek:
                if c.modele.docteur.x - i.x in range(-1,2) and c.modele.docteur.y -i.y in range(-1,2):
                    vaporiser.append(i)
                    c.modele.score += 5
            for i in vaporiser:
                c.modele.dalek.remove(i)
            del vaporiser[:]
            c.modele.munition -= 1
        print("Il reste ", c.modele.munition, " munition au zappeur!") 
    
    def teleporter(self):
        print("T�l�porteur!")
        print(c.modele.difficulty)
        safe = False
        count = 0
        while safe == False:
            
            count += 1
            if count > 1000:
                if c.modele.difficulty == "1":
                    c.modele.difficulty = "2"
                    print("levelup: difficulté téléporteur = 2 (normal)")
                    count =0
                else:
                    c.modele.difficulty = "3"
                    print("levelup: difficulté téléporteur = 3 (difficile)")
            c.modele.docteur.x = randrange(DIMX)
            c.modele.docteur.y = randrange(DIMY)
            
            if c.modele.difficulty == "3":
                safe=True
            if c.modele.difficulty == "2":
                for i in c.modele.dalek:
                    if c.modele.docteur.x == i.x and c.modele.docteur.y == i.y:
                        safe = False
                        break
                    else:
                        safe = True
            if c.modele.difficulty == "1":
                for i in c.modele.dalek:
                    if c.modele.docteur.x - i.x in range(-2,3) and c.modele.docteur.y -i.y in range(-2,3):
                        safe = False
                        break
                    else:
                        safe = True
                        
            for i in c.modele.ferraille:
                if c.modele.docteur.x == i.x and c.modele.docteur.y == i.y:
                    safe = False
                        
    def action(self, action_saisie):

        if action_saisie[0]=='z':
            Docteur.zapper(self)
        elif action_saisie[0]=='t':
            Docteur.teleporter(self)
        elif action_saisie[0]=='r':
            self.runtoend=True
        elif action_saisie[0]=='d':
            self.runtodead=True
        else:            
            self.x+=action_saisie[0]                
            self.y-=action_saisie[1]
       

class Dalek():
    def __init__(self,x,y):
        self.x=x
        self.y=y
        
    def deplacement(self):
        if self.x < c.modele.docteur.x:
            self.x += 1
        elif self.x > c.modele.docteur.x:
            self.x -= 1
        if self.y < c.modele.docteur.y:
            self.y += 1
        elif self.y > c.modele.docteur.y:
            self.y -= 1            
            
class Jeu(): 
    def __init__(self):
        
        self.dalek=[]
        self.ferraille=[]
        self.hiscores=[]
        self.airedejeu=None
        self.niveau=1
        self.score=0
        self.munition=1
        
        self.preparejeu()
        self.difficulty=0
        self.gameon=True
            
    def preparejeu(self):
        
        airedejeu = []
        self.ferraille=[]
  
        #Board
        for i in range(DIMY):
            ligne=[]
            for j in range(DIMX):
                ligne.append(" - ")
            airedejeu.append(ligne)
        self.airedejeu=airedejeu     
        
        #Dalek
        pos=[]
        nbDalek= 5 * self.niveau
        while nbDalek:
            x=random.randrange(DIMX)
            y=random.randrange(DIMY)
            if [x,y] not in pos:
                pos.append([x,y])
                nbDalek-=1
                self.dalek.append(Dalek(x, y))  
        
        
        for i in self.dalek:
            self.airedejeu[i.y][i.x]=" X "      
                                                
        #Docteur
        jettonDocteur=1
        while jettonDocteur:
            x=random.randrange(DIMX)
            y=random.randrange(DIMY)    
            if [x,y] not in pos:
                pos.append([x,y])
                jettonDocteur-=1

        self.docteur=Docteur(x,y)                          
        self.airedejeu[self.docteur.y][self.docteur.x]=" O "
        
        #Enleve ferraille
        for i in self.ferraille:
            self.ferraille.remove(i)
           
        return airedejeu
    
    def miseajourjeu(self):
        
        #Board
        for i in range(DIMY):
            for j in range(DIMX):
                self.airedejeu[j][i]= ' - '
        
        #Docteur
        self.airedejeu[self.docteur.y][self.docteur.x]=" O "
        
        #Dalek
        for i in self.dalek:
            self.airedejeu[i.y][i.x]=" X "
        
        #Ferraille
        for i in self.ferraille:
            self.airedejeu[i.y][i.x]= " F "
   
    def reset(self):
        c.modele.niveau=1
        c.modele.munition=1
        c.modele.score=0
        c.modele.docteur.runtodead==False
        c.modele.docteur.runtoend==False
        del self.dalek[:]
        del self.ferraille[:]
        
class Controlleur():
    def __init__(self):
        self.modele=Jeu()
        self.vue=Vue(self)
        
    def action_saisie(self, action_saisie): 
        self.modele.docteur.action(action_saisie)
        
    def start(self):
            while c.modele.difficulty != "q":
    
                c.modele.gameon =True
                c.modele.docteur.runtoend==False
                c.modele.difficulty = 0
    
                while c.modele.difficulty not in ["1","2","3"]:
                    c.vue.menu_initial()
                    if c.modele.difficulty == "q":
                        exit(0)
                    elif c.modele.difficulty == "h":
                        c.vue.showscores()  
                while c.modele.gameon == True:
            
                    c.vue.imprimeairedejeu(c.modele.airedejeu)
            
                    if c.modele.docteur.runtodead==True:
                        Tour.prochaintour(dicodirection["5"])
                    elif c.modele.docteur.runtoend==True:   
                        for i in c.modele.dalek:
                            if c.modele.docteur.x - i.x in range(-1,2) and c.modele.docteur.y -i.y in range(-1,2):
                                print("Vous allez mourir!Bougez!")
                                c.modele.docteur.runtoend=False
                                Tour.prochaintour(c.vue.menu_partie())
                        if c.modele.docteur.runtoend==True:
                            Tour.prochaintour(dicodirection["5"])                
                 
                    else:
                        Tour.prochaintour(c.vue.menu_partie())
                c.modele.reset()
                c.modele.preparejeu()
        
if __name__ == '__main__':
    
    c=Controlleur()
    c.start()

## -*_ coding: utf-8 -*-

""" 
Salut Alex! J'ai continué a partir de ton fichier. J'ai fini le 
téléporteur ainsi que la zapper. Les collisions interDalek et ferraille 
aussi. Prochaine chose que je vais essayer de faire et l'augmentation 
des niveau avec plus de dalek et  enleve les ferrailles (il a un bug 
qui laisse toujours 1 ferraille). Jai eu beacoup de misere avec 
bitbucket donc pour communiquer envoie moi un courriel a 
benjaminbarry1@gmail.com ca va etre plus simple

"""


import random
import os
from decorator import append
from numpy import empty
from random import randrange
from test.test_winsound import safe_Beep

DIMX=12
DIMY=12

dicodirection = { "1" : [-1,-1], 
                  "2" : [ 0,-1],
                  "3" : [ 1,-1],
                  "4" : [-1, 0],
                  "5" : [ 0, 0],
                  "6" : [ 1, 0],
                  "7" : [-1, 1],
                  "8" : [ 0, 1],
                  "9" : [ 1, 1],
                  "z" : [ 'z', 'z'],
                  "t" : [ 't', 't']}

class Vue():
    def __init__(self,parent):
        self.parent=parent
        
    def imprimeairedejeu(self, aire):

        for i in range(DIMX):
            for j in range(DIMY):
                print(aire[i][j], end='')
            print()          
            
    def menu_initial(self):
        print("Pour d�buter une nouvelle partie, choisir le mode(1=facile, 2=normal, 3= difficile): ")
        c.modele.difficulty = input()
        #score=0
       # niveau=1
       # nbDaleks=5
        #nbFerraille=0
        
    def menu_partie(self):
        print("d�placement / zapper / t�l�porteur : ")
        touche_saisie=input()
        while touche_saisie not in dicodirection:
            touche_saisie=input("reessayer: ")
            
        action_saisie = dicodirection[touche_saisie]

        Tour.prochaintour(action_saisie)
        
        
        
# vraiment pas sur qu'on a besoin d'une classe de la sorte....   ?????  
# mais c'est surement plus clair comme �a.      
class Tour():
    def __init__(self):
        pass
    
    def prochaintour(action_saisie):
    #1 action Docteur
#     self.modele.docteur.action(action_saisie)
        c.action_saisie(action_saisie)
    
    #2 deplacements des Daleks
        for i in c.modele.dalek:
            Dalek.deplacement(i)
        
    #3 verifier collisions Dalek - Dr (fin de partie)
    
        for i in c.modele.dalek:
            if c.modele.docteur.x == i.x and c.modele.docteur.y ==i.y:
                c.modele.gameon=False
                print("GAME OVER")

    #4 verifier collision interDalek
        collisionDalek=[]
        for i in c.modele.dalek:
            for j in c.modele.dalek:
                if (i.x == j.x)and(i.y == j.y):
                    if i != j:
                        collisionDalek.append(i)
                        c.modele.ferraille.append(i)
        for i in collisionDalek:
            c.modele.dalek.remove(i)
            collisionDalek.remove(i)
            
    #5 verifier collision avec tas ferraille
        collisionFerraille=[]
        for m in c.modele.dalek:
            for j in c.modele.ferraille:
                if m.x==j.x and m.y == j.y:
                    collisionFerraille.append(m)
        for i in collisionFerraille:
            c.modele.dalek.remove(i)
            collisionFerraille.remove(i)
    
    #6 verifier s'il reste des Daleks
        if not c.modele.dalek:
            c.modele.docteur.munition+=1
            c.modele.preparejeu()
            
    #7 mise � jour du jeu
        
        c.modele.miseajourjeu()  

            
class Docteur():
    def __init__(self,x,y):
        self.x=x
        self.y=y
        self.munition=1
                   
    def zapper(self):
        print("ZAPPER!")
        if self.munition > 0:
            for i in c.modele.dalek:
                if c.modele.docteur.x - i.x in range(-2,2) and c.modele.docteur.y -i.y in range(-2,2):
                    c.modele.dalek.remove(i) 
    
    def teleporter(self):
        print("T�l�porteur!")
        print(c.modele.difficulty)
        safe = False
        while safe == False:
                
            c.modele.docteur.x = randrange(DIMX)
            c.modele.docteur.y = randrange(DIMY)
            
            if c.modele.difficulty == "3":
                safe=True
            if c.modele.difficulty == "2":
                for i in c.modele.dalek:
                    if c.modele.docteur.x == i.x and c.modele.docteur.y == i.y:
                        safe = False
                        break
                    else:
                        safe = True
            if c.modele.difficulty == "1":
                for i in c.modele.dalek:
                    if c.modele.docteur.x - i.x in range(-2,2) and c.modele.docteur.y -i.y in range(-2,2):
                        safe = False
                        break
                    else:
                        safe = True
                        
            for i in c.modele.ferraille:
                if c.modele.docteur.x == i.x and c.modele.docteur.y == i.y:
                    safe = False
        # � faire...
                        
    def action(self, action_saisie):

        print(action_saisie)

        if action_saisie[0]=='z':
            Docteur.zapper(self)
        elif action_saisie[0]=='t':
            Docteur.teleporter(self)
            
        else:
            
            self.x+=action_saisie[0]
            if (self.x >= DIMX):
                self.x-=1
            if (self.x < 0):
                self.x+=1   
                
            self.y-=action_saisie[1]
            if (self.y >= DIMY):
                self.y-=1
            if (self.y < 0):
                self.y+=1 
            # bon ici je triche un peu, j'empeche de d�passe les limites au lieu de refuser un deplacment illegal...
            # comme le deplacement se fait dans la "VUE", je ne sais pas trop comment verifier si ce deplacement est 
            # legal ou pas.  Pour l'instant �a marche et ca ne plante pas au moins.        



class Dalek():
    def __init__(self,x,y):
        self.x=x
        self.y=y
        
    def deplacement(self):
        if self.x < c.modele.docteur.x:
            self.x += 1
        elif self.x > c.modele.docteur.x:
            self.x -= 1
        if self.y < c.modele.docteur.y:
            self.y += 1
        elif self.y > c.modele.docteur.y:
            self.y -= 1            
            
                        
class Ferraille():
    def __init__(self,x,y):
        self.x=x
        self.y=y
    
class Jeu(): 
    def __init__(self):
#         self.dimx=dimx     # remplacer dimx et dimy par constantes!
#         self.dimy=dimy
        self.dalek=[]
        self.ferraille=[]
        self.airedejeu=None
        self.preparejeu()
        self.difficulty=0
        self.gameon=True

    

        
    def preparejeu(self):
        
        airedejeu = []    # doit �tre modifier en objet !?  voir �nonc� � la fin...
  
        #Board
        for i in range(DIMY):
            ligne=[]
            for j in range(DIMX):
                ligne.append(" - ")
            airedejeu.append(ligne)
        self.airedejeu=airedejeu     
        
        #Dalek
        pos=[]
        nbDalek= 5
        while nbDalek:
            x=random.randrange(DIMX)
            y=random.randrange(DIMY)
            if [x,y] not in pos:
                pos.append([x,y])
                nbDalek-=1
                self.dalek.append(Dalek(x, y))  # on cr�e des objets Dalek !
        
#         for i in pos:
# #             self.dalek.append(Dalek(i[0],i[1])) 
#             iD = 0 # indice Dalek
#             self.dalek[iD] = Dalek(x, y)
#             iD +=1
        
        for i in self.dalek:
            self.airedejeu[i.y][i.x]=" X "
        
        
        #Docteur
        jettonDocteur=1
        while jettonDocteur:
            x=random.randrange(DIMX)
            y=random.randrange(DIMY)    
            if [x,y] not in pos:
                pos.append([x,y])
                jettonDocteur-=1

        self.docteur=Docteur(x,y)   # on cr�e l'objet Docteur ici avec les bonnes coordonn�es
                       
        self.airedejeu[self.docteur.y][self.docteur.x]=" O "   # pas de sur de comprendre pourquoi les x et les y sont invers�s...
        
        #Enleve ferraille
        for i in self.ferraille:
            self.ferraille.remove(i)
           
        return airedejeu
    
    def miseajourjeu(self):
    
        #Board
        for i in range(DIMY):
            for j in range(DIMX):
                self.airedejeu[j][i]= ' - '
        
        #Docteur
        self.airedejeu[self.docteur.y][self.docteur.x]=" O "
        
        #Dalek
        for i in self.dalek:
            self.airedejeu[i.y][i.x]=" X "
        
        #Ferraille
        for i in self.ferraille:
            self.airedejeu[i.y][i.x]= " F "
    
class Controlleur():
    def __init__(self):
        self.modele=Jeu()
        self.vue=Vue(self)
        
    def action_saisie(self, action_saisie): # transmettre l'action recue de la vue au modele
        self.modele.docteur.action(action_saisie)
        

    
if __name__ == '__main__':
    
    #gameon = True   # par opposition � gameover...
    c=Controlleur()
    
    
    c.modele.gameon =True
    c.vue.menu_initial()
    
    while c.modele.gameon == True:
        c.vue.imprimeairedejeu(c.modele.airedejeu)
        c.vue.menu_partie()


    
    

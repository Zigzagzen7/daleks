## -*_ coding: utf-8 -*-

""" 

l'augmentation des niveau avec plus de dalek : DONE
enleve les ferrailles (il a un bug qui laisse toujours 1 ferraille).  DONE
J'ai arrangé le nombre de munitions du zappeur aussi.

- le docteur peut PAS aller sur la Ferraille  : DONE
- verification des coups illegaux: DONE
- corrigé le bug des munitions qui ne s'accumulait pas à chaque niveau. : DONE

Et il reste une couple de features à rajouter:
- score, high scores, run to end, etc.
Mais au moins, on a déjà un jeu fonctionnel...  Bravo!

"""


import random
from random import randrange
import sys
# from test.test_winsound import safe_Beep



# def cls():
#     os.system('cls' if os.name=='nt' else 'clear')



DIMX=12
DIMY=12

dicodirection = { "1" : [-1,-1], 
                  "2" : [ 0,-1],
                  "3" : [ 1,-1],
                  "4" : [-1, 0],
                  "5" : [ 0, 0],
                  "6" : [ 1, 0],
                  "7" : [-1, 1],
                  "8" : [ 0, 1],
                  "9" : [ 1, 1],
                  "z" : [ 'z', 'z'],
                  "t" : [ 't', 't']}

        
        
        


class Vue():
    def __init__(self,parent):
        self.parent=parent
        
    def imprimeairedejeu(self, aire):
        # now, to clear the screen
#         cls()
        print("Score: ", c.modele.score, "   Munitions: ", c.modele.munition)
#         print("Doc X Y : ", c.modele.docteur.x, c.modele.docteur.y)      # coord du doc, pour debug!
        for i in range(DIMX):
            for j in range(DIMY):
                print(aire[i][j], end='')
            print()          
            
    def menu_initial(self):
        print("Pour d�buter une nouvelle partie, choisir le mode(1=facile, 2=normal, 3= difficile)(h pour HIGHSCORES): ")
        c.modele.difficulty = input()
        #score=0
       # niveau=1
       # nbDaleks=5
        #nbFerraille=0
        
    def menu_partie(self):
        print("déplacement / zapper / téléporteur : ")
        touche_valide = False
        
        
        while touche_valide == False:
            touche_saisie=input()
            if touche_saisie not in dicodirection:
                print("choix invalide...  reessayer: ")
            else:
                action_saisie = dicodirection[touche_saisie]
            
                if touche_saisie == "z" and c.modele.munition < 1:
                    print("Il ne vous reste plus de munition au zapper!")
                elif touche_saisie == "t" or touche_saisie == "z":
                    touche_valide = True 
            
                elif int(touche_saisie) in range(1, 10):
            
                    docX= c.modele.docteur.x+action_saisie[0]  # variables temp pour vefifier si position du docteur valide
                    docY= c.modele.docteur.y-action_saisie[1]
#                     print("DocXY: ", docX, docY)                # pour debug
                    touche_valide = True
                
                    if (docX < 0 or docX > DIMX-1): 
                        print("coup invalide")
                        touche_valide = False
                    if (docY < 0 or docY > DIMY-1):
                        print("coup invalide")
                        touche_valide = False
                  
                        # verif si le doc va sur ferraille   
                    for i in c.modele.ferraille:
                        if docX == i.x and docY == i.y:
                            print("coup invalide: vous ne pouvez pas aller sur la ferraille!")
                            touche_valide = False
                                 
        
 

        Tour.prochaintour(action_saisie)
        
    def showscores(self):
        
        c.modele.hiscores.sort(key=None, reverse=True)
        
        for i in c.modele.hiscores:
            if i != 0:
                print(i[1]," : ",i[0])
            
        
# vraiment pas sur qu'on a besoin d'une classe de la sorte....   ?????  
# mais c'est surement plus clair comme �a.      
class Tour():
    def __init__(self):
        pass
    
    def prochaintour(action_saisie):
    #1 action Docteur
#     self.modele.docteur.action(action_saisie)
        c.action_saisie(action_saisie)
    
    #2 deplacements des Daleks
        for i in c.modele.dalek:
            Dalek.deplacement(i)
        
    #3 verifier collisions Dalek - Dr (fin de partie)
    
        for i in c.modele.dalek:
            if c.modele.docteur.x == i.x and c.modele.docteur.y ==i.y:
                c.modele.gameon=False
                print("GAME OVER")
                tempscore=[0," "]
                if len(c.modele.hiscores) < 3 and c.modele.score > 0:
                    tempscore[0]=c.modele.score
                    tempscore[1]=(input("Entrez votre nom: "))
                    c.modele.hiscores.append(tempscore)
                else:
                    for i in c.modele.hiscores:
                        if c.modele.score > i[0]:
                            tempscore[0]=c.modele.score
                            tempscore[1]=(input("Entrez votre nom: "))
                            c.modele.hiscores.pop()
                            c.modele.hiscores.append(tempscore)
                
                c.vue.showscores()
                break

    #4 verifier collision interDalek
    
        collisionDalek=[]
        for i in c.modele.dalek:
            for j in c.modele.dalek:
                if (i.x == j.x)and(i.y == j.y):
                    if i != j:
                        collisionDalek.append(i)
                        c.modele.ferraille.append(i)
        for i in collisionDalek:
            c.modele.dalek.remove(i)
            collisionDalek.remove(i)
            c.modele.score += 5
            
    #5 verifier collision avec tas ferraille
        collisionFerraille=[]
        for m in c.modele.dalek:
            for j in c.modele.ferraille:
                if m.x==j.x and m.y == j.y:
                    collisionFerraille.append(m)    # on ne crée pas d'objet Ferraille comme ça...  à arranger éventuellement!
                    
        for i in collisionFerraille:
            c.modele.dalek.remove(i)
            collisionFerraille.remove(i)
            c.modele.score += 5
    
    #6 verifier s'il reste des Daleks
        if not c.modele.dalek:
            c.modele.munition+=1
            c.modele.niveau += 1
            
            c.modele.preparejeu()
            
    #7 mise � jour du jeu
        if c.modele.gameon == False:
            c.modele.reset()
        c.modele.miseajourjeu()  

            
class Docteur():
    def __init__(self,x,y):
        self.x=x
        self.y=y
#         self.munition=1
                   
    def zapper(self):
        print("ZAPPER!")
        vaporiser=[]
        if c.modele.munition > 0:
            for i in c.modele.dalek:
                if c.modele.docteur.x - i.x in range(-2,2) and c.modele.docteur.y -i.y in range(-2,2):
                    vaporiser.append(i)
                    c.modele.score += 5
            for i in vaporiser:
                c.modele.dalek.remove(i)
                vaporiser.remove(i)
            c.modele.munition -= 1
        print("Il reste ", c.modele.munition, " munition au zappeur!") 
    
    def teleporter(self):
        print("T�l�porteur!")
        print(c.modele.difficulty)
        safe = False
        while safe == False:
                
            c.modele.docteur.x = randrange(DIMX)
            c.modele.docteur.y = randrange(DIMY)
            
            if c.modele.difficulty == "3":
                safe=True
            if c.modele.difficulty == "2":
                for i in c.modele.dalek:
                    if c.modele.docteur.x == i.x and c.modele.docteur.y == i.y:
                        safe = False
                        break
                    else:
                        safe = True
            if c.modele.difficulty == "1":
                for i in c.modele.dalek:
                    if c.modele.docteur.x - i.x in range(-2,2) and c.modele.docteur.y -i.y in range(-2,2):
                        safe = False
                        break
                    else:
                        safe = True
                        
            for i in c.modele.ferraille:
                if c.modele.docteur.x == i.x and c.modele.docteur.y == i.y:
                    safe = False
        # � faire...
                        
    def action(self, action_saisie):

#         print(action_saisie)

        if action_saisie[0]=='z':
            Docteur.zapper(self)
        elif action_saisie[0]=='t':
            Docteur.teleporter(self)
            
        else:            
            self.x+=action_saisie[0]                
            self.y-=action_saisie[1]
       

class Dalek():
    def __init__(self,x,y):
        self.x=x
        self.y=y
        
    def deplacement(self):
        if self.x < c.modele.docteur.x:
            self.x += 1
        elif self.x > c.modele.docteur.x:
            self.x -= 1
        if self.y < c.modele.docteur.y:
            self.y += 1
        elif self.y > c.modele.docteur.y:
            self.y -= 1            
            
                        
class Ferraille():
    def __init__(self,x,y):
        self.x=x
        self.y=y
    
class Jeu(): 
    def __init__(self):
#         self.dimx=dimx     # remplacer dimx et dimy par constantes!
#         self.dimy=dimy
        self.dalek=[]
        self.ferraille=[]
        self.hiscores=[]
        self.airedejeu=None
        self.niveau=1
        self.score=0
        self.munition=1
        
        self.preparejeu()
        self.difficulty=0
        self.gameon=True
        

    

        
    def preparejeu(self):
        
        airedejeu = []    # doit �tre modifier en objet !?  voir �nonc� � la fin...
        self.ferraille=[]
  
        #Board
        for i in range(DIMY):
            ligne=[]
            for j in range(DIMX):
                ligne.append(" - ")
            airedejeu.append(ligne)
        self.airedejeu=airedejeu     
        
        #Dalek
        pos=[]
        nbDalek= 5 * self.niveau
        while nbDalek:
            x=random.randrange(DIMX)
            y=random.randrange(DIMY)
            if [x,y] not in pos:
                pos.append([x,y])
                nbDalek-=1
                self.dalek.append(Dalek(x, y))  # on cr�e des objets Dalek !
        
#         for i in pos:
# #             self.dalek.append(Dalek(i[0],i[1])) 
#             iD = 0 # indice Dalek
#             self.dalek[iD] = Dalek(x, y)
#             iD +=1
        
        for i in self.dalek:
            self.airedejeu[i.y][i.x]=" X "      # on ne passe pas par les objets Dalek, plutot une liste.
                                                # c'est plus simple je l'avoue, mais idealement par les objets je crois.
                                                
                                                
        
        
        #Docteur
        jettonDocteur=1
        while jettonDocteur:
            x=random.randrange(DIMX)
            y=random.randrange(DIMY)    
            if [x,y] not in pos:
                pos.append([x,y])
                jettonDocteur-=1

        self.docteur=Docteur(x,y)   # on cr�e l'objet Docteur ici avec les bonnes coordonn�es
                       
        self.airedejeu[self.docteur.y][self.docteur.x]=" O "   # pas de sur de comprendre pourquoi les x et les y sont invers�s...
        
        #Enleve ferraille
        for i in self.ferraille:
            self.ferraille.remove(i)
           
        return airedejeu
    
    def miseajourjeu(self):
        
        # now, to clear the screen
#         cls()
        #Board
        for i in range(DIMY):
            for j in range(DIMX):
                self.airedejeu[j][i]= ' - '
        
        #Docteur
        self.airedejeu[self.docteur.y][self.docteur.x]=" O "
        
        #Dalek
        for i in self.dalek:
            self.airedejeu[i.y][i.x]=" X "
        
        #Ferraille
        for i in self.ferraille:
            self.airedejeu[i.y][i.x]= " F "
    
    def reset(self):
        c.modele.niveau=1
        c.modele.munition=1
        c.modele.score=0
        
        for i in self.dalek:
            c.modele.dalek.remove(i)
        for i in self.ferraille:
            c.modele.ferraille.remove(i)
class Controlleur():
    def __init__(self):
        self.modele=Jeu()
        self.vue=Vue(self)
        
    def action_saisie(self, action_saisie): # transmettre l'action recue de la vue au modele
        self.modele.docteur.action(action_saisie)
        

    
if __name__ == '__main__':
    
    #gameon = True   # par opposition � gameover...
    c=Controlleur()
    choixmenu = ["1","2","3"]
    
    while c.modele.difficulty != "q":
    
        c.modele.gameon =True
        c.modele.difficulty = 0
    
        while c.modele.difficulty not in choixmenu:
            c.vue.menu_initial()
            if c.modele.difficulty == "q":
                exit(0)
            elif c.modele.difficulty == "h":
                c.vue.showscores()  
        while c.modele.gameon == True:
            c.vue.imprimeairedejeu(c.modele.airedejeu)
            c.vue.menu_partie()


    
    
